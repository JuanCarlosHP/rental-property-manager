# rental-property-manager

A web application for managing rental properties

#Requirements
Must have Nodes.js installed

#Getting Started
1. Install Yarn
2. Start MongoDB: `sudo service mongod start`
3. Run application in dev mode: `yarn dev`
